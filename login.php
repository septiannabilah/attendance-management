<?php
session_start();
require 'functions.php';

if (isset($_SESSION["login"])) {
    
    header("Location: index.php");
    exit;
}

if (isset($_POST["login"])){

    $nip = $_POST["nip"];
    $password = $_POST["password"];

    // cek username di database
    // mengambil data yang ada pada database sesuai username
    $userDB = mysqli_query($conn, "SELECT * FROM user WHERE username = '$nip'");

    // nilai 1 untuk adanya data dalam database 
    // nilai 0 tidak ada data dalam database 
    if (mysqli_num_rows($userDB) === 1) {

        // membuka data dari user di database 
        $row = mysqli_fetch_assoc($userDB);
        
        var_dump($row["id_pegawai"]);

        if ($password === $row["password"]) {
            $id = $row["id_pegawai"];
            $admin = $row["admin?"];
            
            $_SESSION["login"] = true;
             
            if ($admin > 0) {
                header("Location: data_pegawai.php?id=$id");
                exit;
            }

            header("Location: index.php?id=$id");
            exit;
        }
        
        // cek password
        // if ( password_verify($password, $row["password"])){

        //     // set login session menjadi true
        //     $_SESSION["login"] = true; 

        //     header("Location: index.php");
        //     exit;
        // } 
    }

    $error = true;

}



?>




<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1>Log In Page</h1>

    <?php if (isset($error)) : ?> 
        <p style="color: red; font-style: italic;">USERNAME atau PASSWORD SALAH</p>
    <?php endif; ?>

    <form action="" method="post">

        <ul>
                <li>
                <label for="nip">NIP : </label><br>
                <input type="text" name="nip" id="nip">
                </li> <br>

                <li>
                <label for="password">Password : </label><br>
                <input type="password" name="password" id="password">
                </li>

                <!-- <input type="checkbox" name="remember" id="remember">
                <label for="remember">Remember me</label> -->

            <br>
            <button type="submit" name="login">Sign In</button>
            <br>
            <br>

            <label for="register">Doesn't have an account ?</label>
            <a href = "register.php" id="register">Sign Up</a>

        </ul>
    </form>
    
</body>
</html>