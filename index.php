<?php
session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php';

if (isset($_POST["log_out"])) {

    header("Location: logout.php");
    exit;
}

$id_url = $_GET["id"];

// data dari tabel data_pegawai
$row = query("SELECT * FROM data_pegawai WHERE id_pegawai = '$id_url'")[0];
// var_dump($row["nama"]);
// var_dump($id_url);

// data dari tabel absensi 
$absensi = query("SELECT * FROM absensi WHERE id_pegawai = '$id_url'");

// pengambilan data dari absensi ke dalam beberapa kategori status
date_default_timezone_set("Asia/Jakarta");
// check in time status
$in = array_keys(array_column($absensi, 'status'), 'Check In');
$subin = count($in, 1);
$last_id = array_slice($in, -1, 1);
$lastTime = $absensi[$last_id[0]]["create_date"];
$lastCekIn = explode((" "), $lastTime);

// var_dump($subin);
// var_dump($lastCekIn[0]);

// check out time status
$out = array_keys(array_column($absensi, 'status'), 'Check Out');
$subout = count($out, 1);

// break out time status
$bout = array_keys(array_column($absensi, 'status'), 'Break Out');
$countbout = count($bout, 1);
// var_dump($bout);

// break in time status
$bin = array_keys(array_column($absensi, 'status'), 'Break In');
$countbin = count($bin, 1);
// var_dump($bin);

// today time (now)
$today = date('Y-m-d');

if (empty($_POST)){
    $status = "idle"; 
    // status = 1 -> check in, status = 2 -> check out, status = 3 -> break out, status = 4 -> break in
    $checkin = false; 
    $breakout = false;

}

if (isset($_POST["cek_in"])) {
    
    if ($lastCekIn[0] === $today){
        $checkin = false;
        $breakout = false; 
        echo "
            <script>
                alert('TELAH MELAKUKAN CHECK IN HARI INI !');
            </script>
        ";
    } else {
        // $status = "check in";
        $_POST["status"] = "CHECK IN";
    
        if (absensi($_POST) > 0) {
            $checkin = true;
            $breakout = false;
            echo "
                <script>
                    alert('Berhasil melakukan CHECK IN !');
                </script>
            ";
        }
        else {
            echo "
                <script>
                    alert('Gagal melakukan CHECK IN !');
                </script>
            ";
            echo mysqli_error($conn);
        }
    }
}

if (isset($_POST["cek_out"])) {
    $checkin = false;
    // $status = "check out"; 
    $_POST["status"] = "CHECK OUT";

    if (absensi($_POST) > 0) {
        echo "
            <script>
                alert('Berhasil melakukan CHECK OUT !');
            </script>
        ";
    }
    else {
        echo "
            <script>
                alert('Gagal melakukan CHECK OUT !');
            </script>
        ";
        echo mysqli_error($conn);
    }
    
}

if (isset($_POST["break_out"])) {
    // $status = "break out";
    $_POST["status"] = "BREAK OUT"; 
    
    if (absensi($_POST) > 0) {
        $checkin = true;
        $breakout = true;
        echo "
            <script>
                alert('Berhasil melakukan BREAK OUT !');
            </script>
        ";
    }
    else {
        echo "
            <script>
                alert('Gagal melakukan BREAK OUT !');
            </script>
        ";
        echo mysqli_error($conn);
    }
} elseif (isset($_POST["break_in"])) {
    // $status = "break in";
    $_POST["status"] = "BREAK IN"; 
    
    if (absensi($_POST) > 0) {
        $checkin = true;
        $breakout = false;
        echo "
            <script>
                alert('Berhasil melakukan BREAK IN !');
            </script>
        ";
    }
    else {
        echo "
            <script>
                alert('Gagal melakukan CHECK IN !');
            </script>
        ";
        echo mysqli_error($conn);
    }
}

$brout1 = $absensi[1]["create_date"];
$brout2 = $absensi[5]["create_date"];
$brout3 = $absensi[9]["create_date"];

$brin1 = $absensi[2]["create_date"];
$brin2 = $absensi[6]["create_date"];
$brin3 = $absensi[10]["create_date"];

// $date1 = date_create("$brout");
// $date2 = date_create("$brin");
// $diff = date_diff($date1,$date2);

$timein1 = strtotime("$brin1");
$timein2 = strtotime("$brin2");
$timein3 = strtotime("$brin3");

$timeout1 = strtotime("$brout1");
$timeout2 = strtotime("$brout2");
$timeout3 = strtotime("$brout3");

$break1 = $timein1 - $timeout1;
$break2 = $timein2 - $timeout2; 
$break3 = $timein3 - $timeout3;
// var_dump($break1);
// var_dump($break2);
// var_dump($break3);
$breTot = $break1 + $break2 + $break3;

// $breTot = ($timein1 + $timein2 + $timein3) - ($timeout1 + $timeout2 + $timeout3); 

// echo $breTot;

// var_dump($time1);

// echo $diff->format("%i minutes");

// var_dump($absensi[$bout[1]]["create_date"]);

// $j = 0;
// $arrbreout = array();
// while ($j < $countbout) {
//     $valuebreout = $bout[$j];
//     $breout = $absensi[$valuebreout]["create_date"];
//     array_push($arrbreout, "$breout");
//     $explbreakout = explode((" "), $breout);
//     // var_dump($breout);
//     $j++;
// }
// var_dump($arrbreout);

// var_dump($slice);

// var_dump($heck);
// var_dump(date_create($d));

// echo date($heck, $d) . "<br>";
// $explodeThis = explode((" "), $heck);
// $dateThis =  date_create($explodeThis[0]);
// var_dump($dateThis);
// $format = date_format($dateThis, "l, j F Y");
// echo $format . "<br>";

// $last_id = mysqli_insert_id();

// var_dump($explodeThis[0]);

// var_dump($last_id[0]);

// var_dump(date('Y-m-d'));

// var_dump($today);

// var_dump($tomorrow);

// echo gmdate("l, j F Y", $tomorrow) . "<br>";

// var_dump($bout);

// var_dump(new \DateTime('tomorrow'));

// var_dump($checkin);


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Absensi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1>Attendance</h1>

    <h2>Welcome, <?= $row["nama"]; ?></h2>

    <form action="" method="post">

        <?php if (!$checkin) : ?>
            <button type="submit" name="cek_in">Check In </button> <br> <br>
        <?php elseif ($checkin && !$breakout) : ?> 
            <button type="submit" name="cek_out">Check Out</button> <br> <br>
        <?php endif ?>

        <?php if ($checkin && !$breakout) : ?>
            <button type="submit" name="break_out">Break Out</button> <br> <br>
        <?php elseif ($checkin && $breakout) : ?> 
            <button type="submit" name="break_in">Break In</button> <br> <br>
        <?php endif ?>

        <input type="hidden" name="id_pegawai" value="<?= $id_url; ?>">

        <button type="submit" name="log_out">Sign Out</button> <br> <br>
    </form>

    <table border="1" cellpading="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Date</th>
            <th>Check In</th>
            <th>Break (Total)</th>
            <th>Check Out</th>
            <th>Information</th>
            <th>Working Hours (Total)</th>
        </tr>
        
        <?php $i = 0; ?>
        <?php while ($i < $subin) : ?>
        <tr>
            <td><?= $i + 1; ?></td>
            <td>
                <?php
                    $valuein = $in[$i];
                    // print_r($absensi[$valuein]["create_date"]); 
                    $try = $absensi[$valuein]["create_date"];
                    $explode = explode((" "), $try);
                    $date =  date_create($explode[0]);
                    echo date_format($date, "l, j F Y");
                ?>
            </td>
            <td>
                <?php 
                    $time = date_create($explode[1]); 
                    echo date_format($time, "h:i A");
                ?>
            </td>
            <td>
                <?php
                    $totBreak = 0;
                    for ($k = 0; $k < $countbin; $k++) {
                        $valuebreout = $bout[$k];
                        $breout = $absensi[$valuebreout]["create_date"];
                        $valuebrein = $bin[$k];
                        $brein = $absensi[$valuebrein]["create_date"];
                        // array_push($arrbreout, "$breout");
                        $explbreakout = explode((" "), $breout);
                        
                        if ($explode[0] === $explbreakout[0]){
                            $bIn = strtotime("$brein");
                            $bOut = strtotime("$breout");
                            
                            $timeBreak = $bIn - $bOut;
                            $totBreak += $timeBreak;

                            // echo $totBreak . "<br>";
                        } 
                        // else {
                        //     $totBreak = 0;
                        // }
                        
                    }
                    $init = $totBreak;
                    $hours = floor($init / 3600);
                    $minutes = floor(($init / 60) % 60);
                    $seconds = $init % 60;
                    echo "$hours hours $minutes minutes $seconds seconds";

                    // echo date("i", $totBreak) . " minutes " . date("s", $totBreak) . "  seconds" . "<br>";
                ?>
            </td>
            <td>
                <?php
                    if ($i < $subout){
                        $valueout = $out[$i];
                        $tryout = $absensi[$valueout]["create_date"];
                        $explodeout = explode((" "), $tryout);
                        $timeout = date_create($explodeout[1]); 
                        echo date_format($timeout, "h:i A");
                    } else {
                        echo "  ";
                    }
                    
                ?>
            </td>
            <td></td>
            <td>
                <?php
                    if ($i < $subout){
                        $chOut = strtotime("$tryout");
                        $chIn = strtotime("$try");
                        $working = $chOut - $chIn;
                        $totWork = $working - $totBreak;
    
                        $init = $totWork;
                        $hours = floor($init / 3600);
                        $minutes = floor(($init / 60) % 60);
                        $seconds = $init % 60;
                        echo "$hours hours $minutes minutes $seconds seconds";
                    }
                    else {
                        echo " ";
                    }

                    // echo date("i", $totWork) . " minutes " . date("s", $totWork) . " second ";
                ?>
            </td>
            
        </tr>
            <?php $i++; ?>
        <?php endwhile; ?>
    
    </table>
    
</body>
</html>