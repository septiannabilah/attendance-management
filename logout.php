<?php
session_start();

// mengosongkan array $_SESSION
$_SESSION = [];
session_unset();
session_destroy();

header("Location: login.php");
exit;

?>