<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h1>Registration Page</h1>

    <form action="" method="post">

        <ul>
            <li>
                <label for="username">username :</label> <br>
                <input type="text" name="username" id="username" autocomplete="off">
            </li> <br>
            <li>
                <label for="password">password :</label> <br>
                <input type="password" name="password" id="password">    
            </li><br>
            <li>
                <label for="password2">password confirmation :</label> <br>
                <input type="password" name="password2" id="password2">    
            </li><br>
            <br>

                <button type="submit" name="register">Register</button>

        </ul>


    </form>
    
</body>
</html>