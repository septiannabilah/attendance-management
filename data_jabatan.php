<?php
session_start();

if (!isset($_SESSION["login"])) {

    header("Location: login.php");
    exit;
}


require 'functions.php';

$datajabatan = query("SELECT * FROM jabatan");
    
if (isset($_POST["log_out"])) {

    header("Location: logout.php");
    exit;
}

if (isset($_POST["pegawai"])) {

    header("Location: data_pegawai.php");
    exit;
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Jabatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>

    <h1>Data Jabatan</h1>

    <form action="" method="post">
        <button type="submit" name="log_out">Sign Out</button> <br> <br>
        <button type = "submit" name = "pegawai">Data Pegawai</button> <br> <br>
    </form>

    <a href ="tambahjabatan.php">Tambahkan Data Jabatan</a> <br> <br>

    <table border="1" cellpading="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Nama Jabatan</th>
            <th>Jam Kerja</th>
        </tr>
        
        <?php $i = 1; ?>
        <?php foreach ($datajabatan as $jbt) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a href ="ubhjabatan.php?id=<?= $jbt["id_jabatan"]; ?>">ubah</a> |
                <a href ="hpsjabatan.php?id=<?= $jbt["id_jabatan"]; ?>"onclick="return confirm
                ('Yakin ingin Menghapus data tersebut?')">hapus</a>
            </td>
            <td><?= $jbt["nama_jabatan"]; ?></td>
            <td><?= $jbt["jam_kerja"]; ?> jam</td>
            
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    
    </table>
    
</body>
</html>