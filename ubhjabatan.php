<?php
require 'functions.php';
// koneksi ke database
//$conn = mysqli_connect("localhost", "root", "", "mahasiswa");

// ambil data di URL 
$id = $_GET["id"];
// query data mahasiswa berdasarkan id
$jabatan = query("SELECT * FROM jabatan where id_jabatan = $id")[0];

// cek apakah tombol ubah sudah ditekan atau belum
if (isset($_POST["ubah"])) {
    // cek data berhasil diubah atau tidak
    if (ubahjabatan($_POST) > 0) {
        echo "
            <script>
                alert('Success changing data!');
                document.location.href = 'data_jabatan.php';
            </script>
        ";
    }
    else {
        echo "
            <script>
                alert('failed changing data!');
                // document.location.href = 'data_jabatan.php';
            </script>
        ";
        echo mysqli_error($conn);
    }
}
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ubah Data Jabatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>

    <h1>Ubah Data Jabatan</h1>

    <form action = "" method = "post">
        <ul>
            <input type="hidden" name="id" value="<?= $jabatan["id_jabatan"]; ?>">
            <li>
                <label for = "namajabatan">Nama Jabatan : </label> <br>
                <input type = "text" name = "namajabatan" id = "namajabatan" value="<?= $jabatan['nama_jabatan']; ?>" required> 
            </li> <br>
            <li>
                <label for = "jam">Jam Kerja : (1 - 99)</label> <br>
                <input type = "text" name = "jam" id = "jam" value="<?= $jabatan['jam_kerja']; ?>" required> 
            </li><br>

            <button type = "submit" name = "ubah">Ubah data!</button>
        </ul>

    </form>
    
</body>
</html>