<?php
session_start();

if (!isset($_SESSION["login"])) {

    header("Location: login.php");
    exit;
}

require 'functions.php';

if (isset($_POST["log_out"])) {

    header("Location: logout.php");
    exit;
}

if (isset($_POST["jabatan"])) {

    header("Location: data_jabatan.php");
    exit;
}

$datapegawai = query("SELECT * FROM data_pegawai");



?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Data Pegawai</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>

<h1>Data Pegawai</h1>

<form action="" method="post">

    <button type="submit" name="log_out">Sign Out</button> <br> <br>
    <button type="submit" name="jabatan">Data Jabatan</button> <br> <br>

</form>

<a href ="tambahpegawai.php">Tambah Data Pegawai Baru</a> <br> <br>

    <table border="1" cellpading="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>NIP</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Jenis Kelamin</th>
            <th>Jabatan</th>
        </tr>
        
        <?php $i = 1; ?>
        <?php foreach ($datapegawai as $pgw) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a href ="ubhpegawai.php?id=<?= $pgw["id_pegawai"]; ?>">ubah</a> |
                <a href ="hpspegawai.php?id=<?= $pgw["id_pegawai"]; ?>"onclick="return confirm
                ('Yakin ingin Menghapus data tersebut?')">hapus</a>
            </td>
            <td><?= $pgw["nip"]; ?></td>
            <td><?= $pgw["nama"]; ?></td>
            <td><?= $pgw["alamat"]; ?></td>
            <td><?= $pgw["kelamin"]; ?></td>
            <td><?php  
                $idjabatan = $pgw["id_jabatan"];
                $jabatan = query("SELECT nama_jabatan FROM jabatan WHERE id_jabatan = '$idjabatan'")[0]; 
                echo $jabatan["nama_jabatan"];
            ?></td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    
    </table>


    
</body>
</html>