<?php
require 'functions.php';
// koneksi ke database
//$conn = mysqli_connect("localhost", "root", "", "mahasiswa");

// ambil data di URL 
$id = $_GET["id"];
// query data mahasiswa berdasarkan id
$pegawai = query("SELECT * FROM data_pegawai where id_pegawai = $id")[0];

$jabatan = query("SELECT * FROM jabatan");

// cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"])) {
    // cek data berhasil diubah atau tidak
    if (ubahpegawai($_POST) > 0) {
        echo "
            <script>
                alert('Success changing data!');
                document.location.href = 'data_pegawai.php';
            </script>
        ";
    }
    else {
        echo "
            <script>
                alert('failed changing data!');
                // document.location.href = 'data_pegawai.php';
            </script>
        ";
        echo mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Data Pegawai</title>
</head>
<body>
    <h1>Ubah Data Pegawai</h1>

    <form action = "" method = "post">
            <input type="hidden" name="id" value="<?= $pegawai["id_pegawai"]; ?>">
        <ul>
            <li>
                <label for = "nip">NIP : </label> <br>
                <input type = "text" name = "nip" id = "nip" value="<?= $pegawai["nip"] ?>" required> 
            </li> <br>
            <li>
                <label for = "nama">Nama : </label> <br>
                <input type = "text" name = "nama" id = "nama" value="<?= $pegawai["nama"] ?>" required> 
            </li> <br>
            <li>
                <label for = "alamat">alamat : </label> <br>
                <input type = "text" name = "alamat" id = "alamat" value="<?= $pegawai["alamat"] ?>" required> 
            </li><br>
            <li>
            Jenis Kelamin : <br>
                <input type="radio" name="kelamin" <?php if (isset($kelamin) && $kelamin=="Laki-laki") echo "checked";?> value="Laki-laki" required>Perempuan
                <input type="radio" name="kelamin" <?php if (isset($kelamin) && $kelamin=="Laki-laki") echo "checked";?> value="Laki-laki" required>Laki-laki
            </li><br>
            <li>
            Jabatan : <br>
                <?php foreach ($jabatan as $jbt) : ?>
                    
                    <input type="radio" name="jabatan" <?php if (isset($jabatan) && $jabatan===$jbt["id_jabatan"]) echo "checked";?> value="<?= $jbt['id_jabatan']; ?>" required> <?= $jbt["nama_jabatan"]; ?> <br>
                <?php endforeach ?>

            </li> <br>

            <button type = "submit" name = "submit">Ubah data!</button>
        </ul>

    </form>
    
</body>
</html>