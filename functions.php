<?php
// connect ke database
$conn = mysqli_connect("localhost", "root", "", "absensi_pegawai");

function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while($row = mysqli_fetch_assoc($result)){
        $rows[] = $row;
    }
    return $rows;
}

function tambahpegawai($data){
    global $conn;

    // ambil data dari tiap field dalam form
    $nip = htmlspecialchars($data["nip"]);
    $nama = htmlspecialchars($data["nama"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $kelamin = ($data["kelamin"]);
    $jabatan = ($data["jabatan"]);

    // query insert data 
    $query = "INSERT INTO data_pegawai 
    VALUES ('', '$nip', '$nama', '$alamat', '$kelamin', '$jabatan')";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function tambahjabatan($data){
    global $conn;

    // ambil data dari tiap field dalam form
    $namajabatan = htmlspecialchars($data["namajabatan"]);
    $jam = htmlspecialchars($data["jam"]);

    // query insert data 
    $query = "INSERT INTO jabatan 
    VALUES ('', '$namajabatan', '$jam')";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function hapuspegawai($id){
    global $conn;
    mysqli_query($conn, 
    "DELETE FROM data_pegawai where id_pegawai = $id");

    return mysqli_affected_rows($conn);
}

function hapusjabatan($id){
    global $conn;
    mysqli_query($conn, 
    "DELETE FROM jabatan where id_jabatan = $id");

    return mysqli_affected_rows($conn);
}

function ubahpegawai($data){
    global $conn;

    // ambil data dari tiap field dalam form
    $idpegawai = $data["id"];
    $nip = htmlspecialchars($data["nip"]);
    $nama = htmlspecialchars($data["nama"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $kelamin = $data["kelamin"];
    $jabatan = $data["jabatan"];

    // query changing data 
    $query = "UPDATE data_pegawai SET
            nip = '$nip',
            nama = '$nama',
            alamat = '$alamat', 
            kelamin = '$kelamin', 
            id_jabatan = '$jabatan'
            where id_pegawai = $idpegawai
            ";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function ubahjabatan($data){
    global $conn;

    // ambil data dari tiap field dalam form
    $idjabatan = $data["id"];
    $namajabatan = htmlspecialchars($data["namajabatan"]);
    $jamkerja = htmlspecialchars($data["jam"]);

    // query changing data 
    $query = "UPDATE jabatan SET
            nama_jabatan = '$namajabatan',
            jam_kerja = '$jamkerja'
            WHERE id_jabatan = $idjabatan
            ";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function sumBreak($sum) {
    static $sum = 0;
    $sum++; 
    return $sum;
}

function absensi($data){
    global $conn;

    // ambil data dari tiap field dalam form
    $id_pegawai = $data["id_pegawai"];
    // status absensi
    $status = $data["status"];

    // $cekin = $data["cek_in"];
    // $cekout = $data["cek_out"];
    // $breakout = $data["break_out"];
    // $breakin = $data["break_in"];

    if($status === "CHECK IN"){
        $query = "INSERT INTO absensi (id_pegawai, status) VALUES ('$id_pegawai', 'Check In')";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
        exit;
    } 
    elseif ($status === "BREAK OUT") {
        $query = "INSERT INTO absensi (id_pegawai, status) VALUES ('$id_pegawai', 'Break Out')";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
        exit;
    } elseif ($status === "BREAK IN") {
        $query = "INSERT INTO absensi (id_pegawai, status) VALUES ('$id_pegawai', 'Break In')";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
        exit;
    } elseif ($status === "CHECK OUT") {
        $query = "INSERT INTO absensi (id_pegawai, status) VALUES ('$id_pegawai', 'Check Out')";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
        exit;
    }

}

?>