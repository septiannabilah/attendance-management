<?php
require 'functions.php';
// koneksi ke database
$conn = mysqli_connect("localhost", "root", "", "absensi_pegawai");

$jabatan = query("SELECT * FROM jabatan");

// cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"])) {
    // cek data berhasil ditambahkan atau tidak
    if (tambahpegawai($_POST) > 0) {
        echo "
            <script>
                alert('Success adding data!');
                document.location.href = 'data_pegawai.php';
            </script>
        ";
    }
    else {
        echo "Failure";
        echo "<br>";
        echo "Please input again";
        echo mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah data Pegawai</title>
</head>
<body>
    <h1>Tambah Data Pegawai</h1>

    <form action = "" method = "post">
        <ul>
            <li>
                <label for = "nip">NIP : </label> <br>
                <input type = "text" name = "nip" id = "nip" required> 
            </li> <br>
            <li>
                <label for = "nama">Nama : </label> <br>
                <input type = "text" name = "nama" id = "nama" required> 
            </li> <br>
            <li>
                <label for = "alamat">alamat : </label> <br>
                <input type = "text" name = "alamat" id = "alamat" required> 
            </li><br>
            <li>
            Jenis Kelamin : <br>
                <input type="radio" name="kelamin" <?php if (isset($kelamin) && $kelamin=="Laki-laki") echo "checked";?> value="Laki-laki" required>Perempuan
                <input type="radio" name="kelamin" <?php if (isset($kelamin) && $kelamin=="Laki-laki") echo "checked";?> value="Laki-laki" required>Laki-laki
            </li><br>
            <li>
            Jabatan : <br>
                <?php foreach ($jabatan as $jbt) : ?>
                    
                    <input type="radio" name="jabatan" <?php if (isset($jabatan) && $jabatan===$jbt["id_jabatan"]) echo "checked";?> value="<?= $jbt['id_jabatan']; ?>" required> <?= $jbt["nama_jabatan"]; ?> <br>
                <?php endforeach ?>

            </li> <br>

            <button type = "submit" name = "submit">Tambah data!</button>
        </ul>

    </form>
    
</body>
</html>