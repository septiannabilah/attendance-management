<?php
require 'functions.php';

// cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"])) {
    // cek data berhasil ditambahkan atau tidak
    if (tambahjabatan($_POST) > 0) {
        echo "
            <script>
                alert('Success adding data!');
                document.location.href = 'data_jabatan.php';
            </script>
        ";
    }
    else {
        echo "Failure";
        echo "<br>";
        echo "Please input again";
        echo mysqli_error($conn);
    }
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tambah Data Jabatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>

    <h1>Tambah Data Jabatan</h1>

    <form action = "" method = "post">
        <ul>
            <li>
                <label for = "namajabatan">Nama Jabatan : </label> <br>
                <input type = "text" name = "namajabatan" id = "namajabatan" required> 
            </li> <br>
            <li>
                <label for = "jam">Jam Kerja : (1 - 99)</label> <br>
                <input type = "text" name = "jam" id = "jam" required> 
            </li><br>

            <button type = "submit" name = "submit">Tambah data!</button>
        </ul>

    </form>
    
</body>
</html>